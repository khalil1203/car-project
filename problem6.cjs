let getData = function (inventory){ 
    let ans =[];
    if(inventory == null || inventory == undefined){
        return ans;
    }
    if(!Array.isArray(inventory)){
        return ans;
    }
let temp = 0;
    for(let index = 0;index<inventory.length;index++){
        let temp2 = inventory[index].car_make.toLowerCase();
        if(temp2 == "bmw" || temp2 == "audi"){
            ans[temp++] = inventory[index];
        }
    }
return ans;
}
module.exports = getData;