let  getData = function (inventory,dataid){
    let ans =[];
    if(inventory == null || inventory == undefined){
        return ans;
    }
    if(dataid == null || dataid == undefined){
             return ans;
         }
    if(!Array.isArray(inventory)){
        return ans;
    }
    if( typeof dataid != 'number'){
        return ans;
    }
    for (let index = 0;index<inventory.length;index++){
        if(inventory[index].id == dataid){
           // console.log("Car 33 is a "+ inventory[index].car_year + " " + inventory[index].car_make + " " + inventory[index].car_model);
            ans[0] = inventory[index]; 
             return ans;
        }
    }
    return ans;
}
module.exports = getData;
