function demo(index,inventory){
    console.log("Last car is a "+ " " + inventory[index].car_make + " " + inventory[index].car_model);
    }
   let getData = function (inventory){
    if(inventory == null || inventory == undefined){
        return;
    }
    if(!Array.isArray(inventory)){
        return;
    }
    else{
        demo(inventory.length-1,inventory);
    }
   }
   module.exports = getData;