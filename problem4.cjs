let getData = function (inventory){
    let ans = [];
    if(inventory == null || inventory == undefined){
        return ans;
    }
    if(!Array.isArray(inventory)){
        return ans;
    }
for(let index = 0;index<inventory.length;index++){
    ans[index] = inventory[index].car_year;
}
return ans;
}
module.exports = getData;