let getData = function (inventory,demo){
    let ans = [];
    if(inventory == null || inventory == undefined){
        return ans;
    }
    if(demo == null || demo == undefined){
        return ans;
    }
   
    if(!Array.isArray(inventory)){
        return ans;
    }
    if(!Array.isArray(demo)){
        return ans;
    }
   
let temp = 0;
    for(let index = 0;index<demo.length;index++){
        if(demo[index]<2000){
            ans[temp++] = inventory[index];
        }
    }
return ans;
}
module.exports = getData;