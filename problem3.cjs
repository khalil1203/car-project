 let getData = function (inventory){
  if(inventory == null || inventory == undefined){
    return;
}
if(!Array.isArray(inventory)){
    return;
}
else{

    let sorted = inventory.sort(function (first, second) {
        first = first.car_model.toLowerCase();
        second = second.car_model.toLowerCase();
        if (first == second) {
            return 0;
          }
          else if(first < second) {
          return -1;
        }
        else{
          return 1;
        }
    });
    return sorted;
  }
}
module.exports = getData;